var set_interval = 250;
var interval;

$('[data-toggle="tooltip"]').tooltip('disable');

getLocation();

function getLocation(){
	if(navigator.geolocation){
		navigator.geolocation.getCurrentPosition(showPosition);
	}
	else{ 
		console.log("Geolocation is not supported by this browser.");
	}
}

function showPosition(position){
	var latitude = position.coords.latitude;
	var longitude = position.coords.longitude;
	
	$.ajax({
		type: 'POST',
		url: 'ajax.php',
		data: {type: 'geolocation', latitude: latitude, longitude: longitude},
		success: function(msg){
			console.log(msg);
		},
		error: function(msg){
			console.log(msg);
		}
	});
}

function downloadVirus(time_interval){
	$('#time').hide();
	$('#downloading').show();

	var seconds = parseInt($('.progress-bar').text());
	interval = setInterval(function () {
		$('.progress-bar').css('width', seconds + '%');
		$('.progress-bar').text(seconds + '%');

		if(seconds == 75){
			$('#download-info').text('Installing virus...');
		}

		if(++seconds > 100){
			$('#download-info').text('Virus installed successfully!');
			$('#btn-cancel').prop('disabled', true);

			clearInterval(interval);

			crackScreen();
		}
	}, time_interval);
}

function startTimer(duration, display){
	var timer = duration, seconds;
	var prepare = setInterval(function () {
		seconds = parseInt(timer % 60, 10);

		display.text(seconds);

		if(--timer < 0){
			clearInterval(prepare);
			
			downloadVirus(set_interval);
		}
	}, 1000);
}

$(document).on('click', '#btn-yes', function(){
	var time = 3,
		display = $('#time');

	$('#question').hide();
	$('#btn-yes').hide();
	$('#time').text('Prepeare yourself');

	startTimer(time, display);
});

$(document).on('click', '#btn-cancel', function(){
	var clicked = $(this).data('click');

	if(clicked == 1){
		$(this).data('click', 2);
		$('[data-toggle="tooltip"]').tooltip('enable');
		$('[data-toggle="tooltip"]').tooltip('show');
	}

	clearInterval(interval);
	set_interval /= 2;
	downloadVirus(set_interval);
});

function crackScreen(){
	$('.overlay').css('z-index', 1071);
	setTimeout(function(){
		$('.overlay').css('opacity', 1);

		setTimeout(function(){
			$('.spooky-img').show();
			var audio = new Audio('./assets/sound/scream.mp3');
			audio.play();

			$.ajax({
				type: 'POST',
				url: 'ajax.php',
				data: {type: 'finished'},
				success: function(msg){
					console.log(msg);
				},
				error: function(msg){
					console.log(msg);
				}
			});
		}, 3000);
	}, 2000);
}