<?php
	session_start();

	require 'vendor/autoload.php';

	use Dotenv\Dotenv;

	$dotenv = Dotenv::create(__DIR__);
	$dotenv->load();

	if(isset($_POST['type'])){
		
		$mysqli = new mysqli($_ENV['DB_HOST'], $_ENV['DB_USER'], $_ENV['DB_PASS'], $_ENV['DB_NAME']);

		if($mysqli->connect_errno){
			printf("Connect failed: %s\n", $mysqli->connect_error);
			exit();
		}

		if($_POST['type'] == 'geolocation'){
			if(isset($_SESSION['visit'])){
				$mysqli->query("UPDATE t_visitors SET latitude = '".$_POST['latitude']."', longitude = '".$_POST['longitude']."' WHERE visitor_id = ".$_SESSION['visit']);

				$mysqli->close();
			}
		}
		elseif($_POST['type'] == 'get-detail-location'){
			$json = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$_POST['latitude'].','.$_POST['longitude'].'&sensor=false&key=AIzaSyAMZ7_r9iycMvxyIjObbuN6JI8ZxXEaY9w');
			echo $json;
		}
		// elseif($_POST['type'] == 'get-list-visitor'){
		// 	$columns = array( 
		// 		0 => 'timestamp', 
		// 		1 => 'ip_address',
		// 		2 => 'coordinate',
		// 		3 => 'browser',
		// 	);

		// 	$sql = $mysqli->query("SELECT COUNT(*) AS total_data FROM t_visitors");
		// 	$res = $sql->fetch_object();
		// 	$totalData = $res->total_data;

		// 	$totalFiltered = $totalData; 

		// 	$limit = $_POST['length'];
		// 	$start = $_POST['start'];
		// 	$order = $columns[$_POST['order'][0]['column']];
		// 	$dir = $_POST['order'][0]['dir'];

		// 	if(empty($_POST['search']['value'])){
		// 		$rows = $mysqli->query("SELECT * FROM t_visitors ORDER BY {$order} {$dir} LIMIT {$limit} OFFSET {$start}");
		// 	}
		// 	else {
		// 		$search = $_POST['search']['value'];

		// 		$rows = $mysqli->query("SELECT * FROM t_visitors WHERE timestamp LIKE '%{$search}%' OR ip_address LIKE '%{$search}%' OR browser LIKE '%{$search}%' ORDER BY {$order} {$dir} LIMIT {$limit} OFFSET {$start}");

		// 		$sql = $mysqli->query("SELECT COUNT(*) AS total_data FROM t_visitors WHERE timestamp LIKE '%{$search}%' OR ip_address LIKE '%{$search}%' OR browser LIKE '%{$search}%' ORDER BY {$order} {$dir} LIMIT {$limit} OFFSET {$start}");
		// 		$totalFiltered = $sql->fetch_object()->total_data;
		// 	}

		// 	$data = array();
		// 	if($rows){
		// 		while($row = $rows->fetch_object()){
		// 			$nestedData['timestamp'] = date_format(date_create($row->timestamp), 'Y/m/d H:i:s');
		// 			$nestedData['ip_address'] = "<a href='#' data-toggle='modal' data-target='#modal-detail' data-tipe='ip' data-ip='".$row->ip_address."'>".$row->ip_address."</a>";
		// 			if($row->latitude != NULL and $row->longitude != NULL){
		// 				$nestedData['coordinate'] = "<a href='#' data-toggle='modal' data-target='#modal-detail' data-tipe='coordinate' data-latitude='".$row->latitude."' data-longitude='".$row->longitude."'>Lihat Detail</a> | <a href='https://www.google.com/maps/?q=".$row->latitude.",".$row->longitude."' target='_blank'>Open in maps</a>";
		// 			}
		// 			else{
		// 				$nestedData['coordinate'] = "-";
		// 			}
		// 			$nestedData['browser'] = $row->browser;

		// 			$data[] = $nestedData;
		// 		}
		// 	}

		// 	$json_data = array(
		// 		"draw"            => intval($_POST['draw']),  
		// 		"recordsTotal"    => intval($totalData),  
		// 		"recordsFiltered" => intval($totalFiltered), 
		// 		"data"            => $data   
		// 	);

		// 	echo json_encode($json_data); 
		// }
		elseif($_POST['type'] == 'get-ip-detail'){
			$ch = curl_init('ipinfo.io/'.$_POST['ip'].'?token=87fb8bf004b265');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($ch);
			curl_close($ch);

			$response = json_decode($response);
			$json = "<table class='table table-hover table-striped'>";
			foreach($response as $key => $value){
				$json .= "<tr>";
				$json .= "<th>".$key."</th>";
				$json .= "<td>:</td>";
				$json .= "<td>".$value."</td>";
				$json .= "</tr>";
			}
			$json .= "</table>";

			echo json_encode(['html' => $json]);
		}
		elseif($_POST['type'] == 'finished'){
			$_SESSION['finished'] = true;

			$mysqli->query("UPDATE t_visitors SET finished = 1 WHERE visitor_id = ".$_SESSION['visit']);

			$mysqli->close();

			$curl = curl_init();

		    curl_setopt($curl, CURLOPT_URL, 'https://bot.wrino.id/new-visitor');
		    curl_setopt($curl, CURLOPT_POST, 1);
		    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(['text' => '<b>Visitor #'.$_SESSION['visit'].'</b> has finished the game.']));
		    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		        "Accept: */*",
		        "Accept-Encoding: gzip, deflate",
		        "Cache-Control: no-cache",
		        "Connection: keep-alive",
		        "Content-Type: application/json"
		    ));

		    $response = curl_exec($curl);
		    $err = curl_error($curl);

		    curl_close($curl);

			echo json_encode(['html' => '']);
		}
	}
?>