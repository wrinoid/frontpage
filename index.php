<?php
session_start();

require 'vendor/autoload.php';

use Dotenv\Dotenv;

$dotenv = Dotenv::create(__DIR__);
$dotenv->load();

// connect to database
$mysqli = new mysqli($_ENV['DB_HOST'], $_ENV['DB_USER'], $_ENV['DB_PASS'], $_ENV['DB_NAME']);

// check connection
if($mysqli->connect_errno){
	printf("Connect failed: %s\n", $mysqli->connect_error);
	exit();
}

$hello = "Hey, glad you are back!";
$magic = "Still want to see the magic?";

// create session
if(empty($_SESSION['visit'])){
	$mysqli->query("INSERT INTO t_visitors (ip_address, browser, referer) VALUES ('".$_SERVER['REMOTE_ADDR']."', '".$_SERVER['HTTP_USER_AGENT']."', '".$_SERVER["HTTP_REFERER"]."')");

	$_SESSION['visit'] = $mysqli->insert_id;

	$hello = "Hi visitor number #".$_SESSION['visit'];
	$magic = "Do you want to see magic?";

	$curl = curl_init();

    curl_setopt($curl, CURLOPT_URL, 'https://bot.wrino.id/new-visitor');
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(['text' => '<b>New Visitor #'.$_SESSION['visit'].'</b>'.PHP_EOL.'IP Address : '.$_SERVER['REMOTE_ADDR'].PHP_EOL.'Referer : '.$_SERVER["HTTP_REFERER"].PHP_EOL.'Browser : '.$_SERVER['HTTP_USER_AGENT']]));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        "Accept: */*",
        "Accept-Encoding: gzip, deflate",
        "Cache-Control: no-cache",
        "Connection: keep-alive",
        "Content-Type: application/json"
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
}

$mysqli->close();
?>
<!DOCTYPE html>
<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121623493-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-121623493-1');
	</script>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="utf-8">
	<meta property='og:url' content='https://wrino.id/' />
	<meta property='og:type' content='website' />
	<meta property='og:title' content='Welcome - wrino.id' />
	<meta property='og:image' content='https://wrino.id/assets/img/logo.png' />
	<meta property='og:description' content='there is nothing here, come back later.' />
	<meta name='twitter:card' content='summary' />
	<meta name='twitter:site' content='@rino_8' />
	<meta name='twitter:title' content='Welcome - wrino.id' />
	<meta name='twitter:image' content='https://wrino.id/assets/img/logo.png' />
	<meta name="Description" content='there is nothing here, come back later.' />

	<title>Welcome - wrino.id</title>

	<link rel="icon" type="image/png" href="assets/img/favicon.png">

	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/css/fonts.css" rel="stylesheet">
	<link href="assets/css/style.css" rel="stylesheet">
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-6042665997204334" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container">
		<?php if(empty($_SESSION['finished'])){ ?>
		<div class="message">
			<div class="row">
				<div class="col-12">
					<p class="text" id="time"><?= $hello ?></p>
					<p class="text-light" id="question"><?= $magic ?></p>
					<button class="btn btn-warning" id="btn-yes">YES!</button>
					<div id="downloading" style="display:none">
						<span id="download-info">Downloading virus...</span>
						<div class="progress">
							<div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
						</div>
						<button class="btn btn-outline-secondary mt-1" id="btn-cancel" data-click="1">Cancel</button>
					</div>
				</div>
			</div>
		</div>
		<?php } else{ ?>
		<div class="message">
			<div class="row">
				<div class="col-12">
					<p class="text">Thank you for visiting my site.</p>
					<p class="text-light">Have a nice day :)</p>
				</div>
			</div>
		</div>
		<?php } ?>
		<div class="footer">
			<span data-toggle="tooltip" data-placement="top" data-html="true" title="<strong>Did you know?</strong><br><span style='font-size: 12px'>If you click the cancel button, it will make the progress twice faster than before. WOW.</span>">wrino.id</span>
		</div>
	</div>
	<div class="overlay"><img src="assets/img/spooky.png" class="spooky-img"></div>

	<script src="assets/js/jquery-3.3.1.min.js"></script>
	<script src="assets/js/bootstrap.bundle.min.js"></script>
	<script src="assets/js/script.js"></script>
</body>
</html>